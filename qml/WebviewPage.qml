import QtQuick 2.9
import Ubuntu.Components 1.3
import Morph.Web 0.1
import QtWebEngine 1.7

import "components"

Page {
    id: web
    anchors {
        fill: parent
        bottom: parent.bottom
    }
    width: parent.width
    height: parent.height

    header: AppHeader {
        title: "Quixe"
        id: header
    }

    property string game

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Ubuntu; Linux arm; rv:65.0) Gecko/20100101 Firefox/65.0'
        offTheRecord: false
        //userScripts: []
    }

    WebView {
        id: webview

        //This factor should be around 2.75 for mobile
        property real factor: units.gu(1) / 8.4

        anchors {
            fill: parent
            topMargin: header.visible ? header.height : 0//test
            bottom: parent.bottom
        }
        width: parent.width
        height: parent.height

        context: webcontext
        url: Qt.resolvedUrl('www/play.html?story=' + game)

        settings {
            javascriptCanAccessClipboard: true
            localContentCanAccessFileUrls: true
            localContentCanAccessRemoteUrls: true
            allowRunningInsecureContent: true
            allowWindowActivationFromJavaScript : true
        }

        Component.onCompleted: {
            settings.localStorageEnabled = true;
        }

        zoomFactor: factor
    }

    ProgressBar {
        //style: { color: darkColor }
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: header.visible ? header.bottom : parent.top
        }

        showProgressPercentage: false
        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    HideHeader { }

} // </page>
