import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Morph.Web 0.1
import QtWebEngine 1.7
import Ubuntu.DownloadManager 1.2

import Backend 1.0
import "components"

Page {
    id: webArchive

    property string fileName

    anchors {
        fill: parent
        bottom: parent.bottom
    }
    width: parent.width
    height: parent.height

    header: AppHeader {
        title: "If Archive"
        id: headerIf
    }

    property string game

    SingleDownload {
        id: downloadGame

        metadata: Metadata {
            showInIndicator: true
            title: fileName
        }

        onFinished: {
            console.log("FINISHED",path)

            console.log("Path is: " + path, "fileName",fileName)
            var gameName = fileName
            console.log("Copy: ", gameName)
            Backend.copyLocally(path.replace("file://",""), gameName)

            mainPageStack.pop()
        }

        onErrorMessageChanged: {
            console.log("SingleDownload error", errorMessage)
            PopupUtils.open(errorDialog, root.mainView, { "message" : errorMessage })
        }
    }

    WebContext {
        id: webcontextIF
        //userAgent: 'Mozilla/5.0 (Linux; Android 5.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.102 Mobile Safari/537.36 Ubuntu Touch Webapp'

        userScripts: [
            WebEngineScript {
                id: cssinjection
                injectionPoint: WebEngineScript.DocumentCreation
                sourceUrl: Qt.resolvedUrl('js/cssinjection.js')
                worldId: WebEngineScript.MainWorld
            }
        ]

        onDownloadRequested: {
            console.log('download requested', download.url, download.downloadFileName);
            console.log("downloadDirectory",download.downloadDirectory)

            PopupUtils.open(downloadingDialog, root.mainView, { "fileName" : download.downloadFileName }) //path })
            webArchive.fileName = download.downloadFileName || i18n.tr("unknown name")
            downloadGame.download(download.url)
        }
    }

    WebView {
        id: webview

        //This factor should be around 2.75 for mobile
        property real factor: units.gu(1) / 8.4

        anchors {
            fill: parent
            topMargin: header.visible ? header.height : 0//test
            bottom: parent.bottom
        }
        width: parent.width
        height: parent.height

        context: webcontextIF
        url: 'http://www.ifarchive.org/indexes/if-archiveXgamesXglulx.html'

        settings {
            javascriptCanAccessClipboard: true
            localContentCanAccessFileUrls: true
            localContentCanAccessRemoteUrls: true
            allowRunningInsecureContent: true
            allowWindowActivationFromJavaScript : true
        }

        Component.onCompleted: {
            settings.localStorageEnabled = true;
        }

        zoomFactor: factor
    }

    ProgressBar {
        height: units.dp(3)

        anchors {
            left: parent.left
            right: parent.right
            top: headerIf.visible ? headerIf.bottom : parent.top
        }

        showProgressPercentage: false
        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    HideHeader { }

    Component {
        id: downloadingDialog

        DownloadingDialog {
            anchors.fill: parent

            onAbortDownload: downloadGame.cancel()

            percentage: downloadGame.progress
        }
    }

    Component {
        id: errorDialog

        ErrorDialog {
            anchors.fill: parent
        }
    }
}
